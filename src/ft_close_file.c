/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_close_file.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/09 23:31:43 by graybaud          #+#    #+#             */
/*   Updated: 2014/03/29 12:23:34 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>
#include <libft.h>
#include "define.h"
#include "wolf3d.h"

void	ft_close_file(t_data *data)
{
	if (close(X) == -1)
	{
		ft_putendl_fd(ERROR_CLOSE, 2);
		exit(EXIT_FAILURE);
	}
}
