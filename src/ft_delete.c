/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_delete.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/04 12:58:12 by graybaud          #+#    #+#             */
/*   Updated: 2014/03/29 12:19:27 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "define.h"
#include "wolf3d.h"

void	ft_delete_matrix(t_data *data)
{
	int		i;

	i = 0;
	if (MATRIX)
	{
		while (i < MAP_SIZE)
			free(MATRIX[i++]);
		free(MATRIX);
		MATRIX = NULL;
	}
}
