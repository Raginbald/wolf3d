/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/02 16:06:07 by graybaud          #+#    #+#             */
/*   Updated: 2014/03/29 12:52:44 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include "wolf3d.h"

int		main(void)
{
	t_data		*data;

	data = ft_init_data();
	ft_init_matrix(data);
	ft_launch_game(data);
	return (0);
}
