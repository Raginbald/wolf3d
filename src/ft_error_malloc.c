/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_error_malloc.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/03 10:57:38 by graybaud          #+#    #+#             */
/*   Updated: 2014/03/29 12:24:22 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <libft.h>
#include "define.h"
#include "wolf3d.h"

void	ft_error_malloc(void)
{
	ft_putendl_fd(ERROR_MALLOC, 2);
	exit(EXIT_FAILURE);
}
