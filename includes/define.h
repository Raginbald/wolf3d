/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   define.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/02 17:04:31 by graybaud          #+#    #+#             */
/*   Updated: 2014/04/15 13:40:25 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef		DEFINE_H
# define	DEFINE_H

# define ERROR_CLOSE	"Error: close() map_wolf failed"
# define ERROR_OPEN		"Error: open() map_wolf failed"
# define ERROR_MALLOC	"Error: allocation memory failed"
# define PATH			"./map_wolf"
# define WIDTH			640
# define HEIGHT			400
# define MAP_SIZE		10
# define MIDH			200
# define RIGHT			65363
# define LEFT			65361
# define UP				65362
# define DOWN			65364
# define ESC			65307
# define R				114
# define F				102
# define D				100
# define G				103
# define SPACE			32
# define MOVE_SPEED		0.0100
# define ROT_SPEED		0.013962634
# define IMG			data->env.img
# define WIN			data->env.win
# define INI			data->env.ini
# define IMG_PTR		data->pixel.img_ptr
# define BPP			data->pixel.bpp
# define SIZELINE		data->pixel.sizeline
# define ENDIAN			data->pixel.endian
# define MATRIX			data->matr
# define WIN_X			data->win_x
# define MAP_X			data->map_x
# define MAP_Y			data->map_y
# define POS_X			data->pos_x
# define POS_Y			data->pos_y
# define DIR_VX			data->dir_vx
# define DIR_VY			data->dir_vy
# define PLAN_X			data->plan_x
# define PLAN_Y			data->plan_y
# define RAY_X			data->ray_x
# define RAY_Y			data->ray_y
# define RAY_VX			data->ray_vx
# define RAY_VY			data->ray_vy
# define RAY_LEN		data->ray_len
# define DELTA_X		data->delta_x
# define DELTA_Y		data->delta_y
# define INTER_X		data->inter_x
# define INTER_Y		data->inter_y
# define STEP_X			data->step_x
# define STEP_Y			data->step_y
# define WALL_H			data->wall_h
# define START			data->start
# define END			data->end
# define X				data->x
# define Y				data->y
# define AXIS			data->axis
# define MOVE_UP		data->move_up
# define MOVE_DOWN		data->move_down
# define MOVE_LEFT		data->move_left
# define MOVE_RIGHT		data->move_right
# define END_GAME		data->end_game
# define WALL			data->wall
# define SKY		0xA9EAFE
# define FLOOR		0xCECECE
# define WALL_S		0xFFFFFF
# define WALL_N		0x000000
# define WALL_E		0x5A3A22
# define WALL_W		0xAE642D

#endif
