/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_open_file.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/09 23:29:05 by graybaud          #+#    #+#             */
/*   Updated: 2014/01/18 16:23:27 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fcntl.h>
#include <stdlib.h>
#include <libft.h>
#include "define.h"
#include "wolf3d.h"

void	ft_open_file(t_data *data)
{
	X = open(PATH, O_RDONLY);
	if (X == -1)
	{
		ft_putendl_fd(ERROR_OPEN, 2);
		exit(EXIT_FAILURE);
	}
}
