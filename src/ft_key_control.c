/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_key_control.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/06 15:51:07 by graybaud          #+#    #+#             */
/*   Updated: 2014/03/29 15:18:44 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <math.h>
#include <stdlib.h>
#include <mlx.h>
#include "define.h"
#include "wolf3d.h"

void	ft_key_esc(t_data *data)
{
	if (MATRIX)
		ft_delete_matrix(data);
	mlx_clear_window(INI, WIN);
	mlx_destroy_window(INI, WIN);
	exit(EXIT_SUCCESS);
}

void	ft_key_up(t_data *data)
{
	if (MATRIX[(int)(POS_X + DIR_VX * MOVE_SPEED)][(int)(POS_Y)] == 0)
		POS_X += DIR_VX * MOVE_SPEED;
	if (MATRIX[(int)(POS_X)][(int)(POS_Y + DIR_VY * MOVE_SPEED)] == 0)
		POS_Y += DIR_VY * MOVE_SPEED;
}

void	ft_key_down(t_data *data)
{
	if (MATRIX[(int)(POS_X - DIR_VX * MOVE_SPEED)][(int)(POS_Y)] == 0)
		POS_X -= DIR_VX * MOVE_SPEED;
	if (MATRIX[(int)(POS_X)][(int)(POS_Y - DIR_VY * MOVE_SPEED)] == 0)
		POS_Y -= DIR_VY * MOVE_SPEED;
}

void	ft_key_right(t_data *data)
{
	double	dir_x;
	double	plan_x;

	dir_x = DIR_VX;
	plan_x = PLAN_X;
	DIR_VX = DIR_VX * cos(-ROT_SPEED) - DIR_VY * sin(-ROT_SPEED);
	DIR_VY = dir_x * sin(-ROT_SPEED) + DIR_VY * cos(-ROT_SPEED);
	PLAN_X = PLAN_X * cos(-ROT_SPEED) - PLAN_Y * sin(-ROT_SPEED);
	PLAN_Y = plan_x * sin(-ROT_SPEED) + PLAN_Y * cos(-ROT_SPEED);
}

void	ft_key_left(t_data *data)
{
	double	dir_x;
	double	plan_x;

	dir_x = DIR_VX;
	plan_x = PLAN_X;
	DIR_VX = DIR_VX * cos(ROT_SPEED) - DIR_VY * sin(ROT_SPEED);
	DIR_VY = dir_x * sin(ROT_SPEED) + DIR_VY * cos(ROT_SPEED);
	PLAN_X = PLAN_X * cos(ROT_SPEED) - PLAN_Y * sin(ROT_SPEED);
	PLAN_Y = plan_x * sin(ROT_SPEED) + PLAN_Y * cos(ROT_SPEED);
}
