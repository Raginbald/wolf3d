# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/01/01 14:11:02 by graybaud          #+#    #+#              #
#    Updated: 2014/04/15 13:13:15 by graybaud         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME	= wolf3d
CC		= cc
FTDIR	= libft
LIBFT	= $(FTDIR)/libft.a
CFLAGS	= -Wall -Wextra -Werror -g3 -O3
IFLAGS	= -I./includes/ -I./libft/includes -I/usr/X11/include
LFLAGS	= -L./libft -lft -L/usr/X11/lib -lmlx -lXext -lX11
RM		= rm -Rf
H		= includes/wolf3d.h			\
		  includes/define.h			\

DIRSRC	= src
DIROBJ	= obj
SRC		= src/main.c				\
		  src/ft_open_file.c		\
		  src/ft_error_malloc.c		\
		  src/ft_close_file.c		\
		  src/ft_delete.c			\
		  src/ft_init.c				\
		  src/ft_key_control.c		\
		  src/ft_draw.c				\
		  src/ft_raycasting.c		\
		  src/ft_mlx.c				\

OBJ		= $(addprefix $(DIROBJ)/, $(SRC:.c=.o))

$(addprefix $(DIROBJ)/, %.o): %.c
	@$(CC) $(CFLAGS) $(IFLAGS) -o $@ -c $<

$(NAME): $(LIBFT) $(OBJ) $(H)
	@echo "building $(NAME) ... "
	@$(CC) $(CFLAGS) $(OBJ) $(LFLAGS) $(IFLAGS) -o $@
	@echo "$(NAME) created ! \n"

$(LIBFT):
	@echo "building $(FTDIR) ... "
	@(cd  $(FTDIR) && $(MAKE))
	@echo "$(FTDIR) created ! \n"

all: $(NAME)

clean:
	@$(RM) $(DEBUG) $(DEBUG).dSYM $(OBJ)
	@(cd  $(FTDIR) && $(MAKE) $@)
	@echo "$(DEBUG), $(DEBUG).dSYM, objects erased !"

fclean:clean
	@$(RM) $(NAME)
	@(cd  $(FTDIR) && $(MAKE) $@)
	@echo "$(NAME), $(FTDIR) erased !\n"

re: fclean all

$(DIROBJ):
	@/bin/mkdir $(DIROBJ);			\
	for DIR in src;					\
	do								\
		/bin/mkdir $(DIROBJ);		\
	done

.PHONY: clean, re
