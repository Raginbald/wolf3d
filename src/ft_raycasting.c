/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_raycasting.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/06 22:36:29 by graybaud          #+#    #+#             */
/*   Updated: 2014/04/15 17:09:50 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <math.h>
#include <stdlib.h>
#include <mlx.h>
#include "define.h"
#include "wolf3d.h"

void	ft_init_calcul_01(t_data *data)
{
	WIN_X = 2 * X / ((double)WIDTH - 1);
	RAY_X = POS_X;
	RAY_Y = POS_Y;
	RAY_VX = DIR_VX + PLAN_X * WIN_X;
	RAY_VY = DIR_VY + PLAN_Y * WIN_X;
	MAP_X = (int)RAY_X;
	MAP_Y = (int)RAY_Y;
	DELTA_X = sqrt(1 + (RAY_VY * RAY_VY) / (RAY_VX * RAY_VX));
	DELTA_Y = sqrt(1 + (RAY_VX * RAY_VX) / (RAY_VY * RAY_VY));
}

void	ft_init_direction(t_data *data)
{
	if (RAY_VX < 0)
	{
		STEP_X = -1;
		INTER_X = (RAY_X - MAP_X) * DELTA_X;
	}
	else
	{
		STEP_X = 1;
		INTER_X  = (MAP_X + 1.0 - RAY_X) * DELTA_X;
	}
	if (RAY_VY < 0)
	{
		STEP_Y = -1;
		INTER_Y = (RAY_Y - MAP_Y) * DELTA_Y;
	}
	else
	{
		STEP_Y = 1;
		INTER_Y = (MAP_Y + 1.0 - RAY_Y) * DELTA_Y;
	}
}

void	ft_cast_ray(t_data *data)
{
	WALL = Y = 0;
	while (WALL == 0)
	{
		if (INTER_X < INTER_Y)
		{
			INTER_X += DELTA_X;
			MAP_X += STEP_X;
			AXIS = 0;
		}
		else
		{
			INTER_Y += DELTA_Y;
			MAP_Y += STEP_Y;
			AXIS = 1;
		}
		if (MATRIX[MAP_X][MAP_Y] > 0)
			WALL = 1;
	}
	if (AXIS == 0)
		RAY_LEN = fabs((MAP_X - RAY_X + (1 - STEP_X) / 2) / RAY_VX);
	else
		RAY_LEN = fabs((MAP_Y - RAY_Y + (1 - STEP_Y) / 2) / RAY_VY);
	WALL_H = abs((int)(HEIGHT / RAY_LEN));
	START = (WALL_H > HEIGHT ? 0 : (MIDH) - (WALL_H / 2));
	END = (WALL_H > HEIGHT ? HEIGHT - 1 : (MIDH) + (WALL_H / 2));
}

int		ft_raycasting(t_data *data)
{
	IMG = mlx_new_image(INI, WIDTH, HEIGHT);
	IMG_PTR = mlx_get_data_addr(IMG, &(BPP), &(SIZELINE), &(ENDIAN));
	X = 0;
	while (X < WIDTH)
	{
		ft_init_calcul_01(data);
		ft_init_direction(data);
		ft_cast_ray(data);
		ft_draw_vertical_line(data);
		X++;
	}
	mlx_put_image_to_window(INI, WIN, IMG, 0, 0);
	mlx_destroy_image(INI, IMG);
	return (0);
}
