/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_draw.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/17 21:51:21 by graybaud          #+#    #+#             */
/*   Updated: 2014/03/29 12:38:05 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include <mlx.h>
#include "define.h"
#include "wolf3d.h"

void	ft_pixel_put(t_data *data, int x, int y, int color)
{
	int		res;

	IMG_PTR = mlx_get_data_addr(IMG, &(BPP), &(SIZELINE), &(ENDIAN));
	res = (x * (BPP / 8) + (y * SIZELINE));
	IMG_PTR[res] = color;
	IMG_PTR[res + 1] = color >> 8;
	IMG_PTR[res + 2] = color >> 16;
}

void	ft_draw_vertical_line(t_data *data)
{
	while (Y < HEIGHT)
	{
		if (Y < START || Y >= 0)
			ft_pixel_put(data, X, Y, SKY);
		if (Y <= END && Y >= START)
		{
			if (AXIS == 0)
			{
				if (RAY_VX > 0)
					ft_pixel_put(data, X, Y, WALL_W);
				else
					ft_pixel_put(data, X, Y, WALL_E);
			}
			else
			{
				if (RAY_VY > 0)
					ft_pixel_put(data, X, Y, WALL_S);
				else
					ft_pixel_put(data, X, Y, WALL_N);
			}
		}
		if (Y > END)
			ft_pixel_put(data, X, Y, FLOOR);
		Y++;
	}
}
