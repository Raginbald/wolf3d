/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wolf3d.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/18 16:03:34 by graybaud          #+#    #+#             */
/*   Updated: 2014/04/15 17:09:10 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef		WOLF3D_H
# define	WOLF3D_H

typedef	struct	s_mlx
{
	void		*ini;
	void		*win;
	void		*img;
}				t_mlx;

typedef	struct	s_color
{
	char		*img_ptr;
	int			bpp;
	int			sizeline;
	int			endian;
}				t_color;

typedef	struct	s_data
{
	int			**matr;
	double		pos_x;
	double		pos_y;
	double		dir_vx;
	double		dir_vy;
	double		plan_x;
	double		plan_y;
	double		inter_x;
	double		inter_y;
	double		delta_x;
	double		delta_y;
	double		ray_len;
	double		ray_x;
	double		ray_y;
	double		ray_vx;
	double		ray_vy;
	double		win_x;
	int			start;
	int			end;
	int			step_x;
	int			step_y;
	int			map_x;
	int			map_y;
	int			wall_h;
	char		move_up;
	char		move_down;
	char		move_left;
	char		move_right;
	char		end_game;
	int			x;
	int			y;
	char		axis;
	int			wall;
	t_color		pixel;
	t_mlx		env;
}				t_data;
/*--------------------ft_ini.c------------------------------------------------*/
t_data	*ft_init_data(void);
void	ft_init_matrix(t_data *data);
/*--------------------ft_error_open_close.c-----------------------------------*/
void	ft_close_file(t_data *data);
void	ft_open_file(t_data *data);
void	ft_error_malloc(void);
void	ft_delete_matrix(t_data *data);
/*--------------------ft_draw.c-----------------------------------------------*/
void	ft_pixel_put(t_data *data, int x, int y, int color);
void	ft_draw_top(t_data *data);
void	ft_draw_wall(t_data *data);
void	ft_draw_floor(t_data *data);
void	ft_draw_vertical_line(t_data *data);
/*--------------------ft_key_control.c----------------------------------------*/
void	ft_key_esc(t_data *data);
void	ft_key_up(t_data *data);
void	ft_key_down(t_data *data);
void	ft_key_left(t_data *data);
void	ft_key_right(t_data *data);
/*--------------------ft_mlx.c------------------------------------------------*/
int		ft_key_press(int key, t_data *data);
int		ft_key_release(int key, t_data *data);
int		ft_loop_hook(t_data *data);
void	ft_launch_game(t_data *data);
/*--------------------ft_raycasting.c-----------------------------------------*/
void	ft_init_calcul_01(t_data *data);
void	ft_init_direction(t_data *data);
void	ft_cast_ray(t_data *data);
int		ft_raycasting(t_data *data);

#endif
