/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_mlx.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/06 20:30:03 by graybaud          #+#    #+#             */
/*   Updated: 2014/04/15 13:25:31 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <mlx.h>
#include <stdlib.h>
#include "define.h"
#include "wolf3d.h"

int		ft_key_press(int key, t_data *data)
{
	if (key == ESC)
		END_GAME = 1;
	if (key == UP || key == R)
		MOVE_UP = 1;
	if (key == DOWN || key == F)
		MOVE_DOWN = 1;
	if (key == RIGHT || key == G)
		MOVE_RIGHT = 1;
	if (key == LEFT || key == D)
		MOVE_LEFT = 1;
	return (0);
}

int		ft_key_release(int key, t_data *data)
{
	if (key == ESC)
		END_GAME = 0;
	if (key == UP || key == R)
		MOVE_UP = 0;
	if (key == DOWN || key == F)
		MOVE_DOWN = 0;
	if (key == RIGHT || key == G)
		MOVE_RIGHT = 0;
	if (key == LEFT || key == D)
		MOVE_LEFT = 0;
	return (0);
}

int		ft_loop_hook(t_data *data)
{
	if (END_GAME)
		ft_key_esc(data);
	if (MOVE_UP)
		ft_key_up(data);
	if (MOVE_DOWN)
		ft_key_down(data);
	if (MOVE_RIGHT)
		ft_key_right(data);
	if (MOVE_LEFT)
		ft_key_left(data);
	ft_raycasting(data);
	return (0);
}

void	ft_launch_game(t_data *data)
{
	INI = mlx_init();
	WIN = mlx_new_window(INI, WIDTH, HEIGHT, "Grimly 3d");
	mlx_hook(WIN, 2, 1, ft_key_press, data);
	mlx_hook(WIN, 3, 2, ft_key_release, data);
	mlx_loop_hook(INI, ft_loop_hook, data);
	mlx_loop(INI);
}
