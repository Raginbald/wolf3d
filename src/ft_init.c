/**************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/04 11:42:44 by graybaud          #+#    #+#             */
/*   Updated: 2014/04/15 13:35:07 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <stdlib.h>
#include "define.h"
#include "wolf3d.h"

t_data	*ft_init_data(void)
{
	t_data	*data;
	int		i;

	if (!(data = (t_data *)malloc(sizeof(t_data))))
		ft_error_malloc();
	POS_X = 2.5;
	POS_Y = 4;
	DIR_VX = -1;
	DIR_VY = 0;
	PLAN_X = 0;
	PLAN_Y = 0.33;
	Y = X = i = 0;
	if (!(MATRIX = (int **)malloc(sizeof(int *) * MAP_SIZE)))
		ft_error_malloc();
	while (i < MAP_SIZE)
	{
		if (!(MATRIX[i] = (int *)malloc(sizeof(int) * MAP_SIZE)))
			ft_error_malloc();
		i++;
	}
	return (data);
}

void	ft_init_matrix(t_data *data)
{
	char	*line;
	int		i;
	int		j;

	ft_open_file(data);
	j = i = 0;
	while (j < MAP_SIZE && (ft_get_next_line(X, &line) != -1))
	{
		i = 0;
		while (i < MAP_SIZE)
		{
			MATRIX[j][i] = (line[i] - 48);
			i++;
		}
		j++;
	}
	ft_close_file(data);
	X = 0;
}
